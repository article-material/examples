import os
import sys
import psycopg2
from dotenv import load_dotenv


load_dotenv()
broadcast_to = 'message_queue_broadcast'
repetitions = int(sys.argv[1])
conn = psycopg2.connect(os.getenv("CONN_STRING"))
cur = conn.cursor()
insert_statement = 'INSERT INTO "JsonMessageQueue" \
                    ("JsonId", "RootId", "Json") \
                    VALUES \
                    (%s, %s, %s);'


def produce_msg(cur, JsonId, RootId, Json):
    cur.execute(insert_statement, (JsonId, RootId, Json))
    cur.execute(f"NOTIFY {broadcast_to};")
    conn.commit()


Json = '{"name": "This is a sample JSON payload"}'


if __name__ == "__main__":
    for n in range(repetitions):
        produce_msg(cur, "1234", "1234", Json)

cur.close()
conn.close()

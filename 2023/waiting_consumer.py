import os
import sys
import logging
import time
import psycopg2
import json
from multiprocessing import Process
from dotenv import load_dotenv


load_dotenv()
log = logging.getLogger()
console = logging.StreamHandler(sys.stdout)
log.addHandler(console)
log.setLevel(logging.INFO)
select_statement = 'SELECT "Id", "JsonId", "RootId", "CreatedOn", "Json" \
                    FROM "JsonMessageQueue" \
                    ORDER BY "Id" LIMIT 1 \
                    FOR UPDATE SKIP LOCKED;'
delete_statement = 'DELETE FROM "JsonMessageQueue" WHERE "Id" = %s;'
consume_statement = 'INSERT INTO "JsonMessageArchive" \
                        ("Id", "JsonId", "RootId", "CreatedOn", \
                        "Json", "ConsumedBy") \
                        VALUES \
                        (%s, %s, %s, %s, %s, %s);'


def consume_msg(name):
    conn = psycopg2.connect(os.getenv("CONN_STRING"))
    while True:
        cur = conn.cursor()
        cur.execute(select_statement)
        result = cur.fetchone()
        if result:
            rs = result
            cur.execute(delete_statement, (rs[0],))
            cur.execute(consume_statement,
                        (rs[0], rs[1], rs[2], rs[3], json.dumps(rs[4]), name))
            conn.commit()
            log.info(f'Consumer {name} consumed message with Id = {rs[0]}')
        else:
            time.sleep(1)


if __name__ == "__main__":
    names = ['Consumer 1', 'Consumer 2']
    procs = []

    # instantiating process with arguments
    for name in names:
        proc = Process(target=consume_msg, args=(name,))
        procs.append(proc)
        proc.start()

    # complete the processes
    for proc in procs:
        proc.join()

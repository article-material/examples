import os
import psycopg2
from dotenv import load_dotenv


load_dotenv()
conn = psycopg2.connect(os.getenv("CONN_STRING"))
cur = conn.cursor()
cur.execute("""
    CREATE TABLE IF NOT EXISTS "JsonMessageQueue"
    (
        "Id" bigserial primary key,
        "JsonId" bigint NOT NULL,
        "RootId" bigint NOT NULL,
        "CreatedOn" timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "Json" json NOT NULL
    )

            """)
conn.commit()
cur.execute("""
    CREATE TABLE IF NOT EXISTS "JsonMessageArchive"
    (
        "Id" bigint primary key,
        "JsonId" bigint NOT NULL,
        "RootId" bigint NOT NULL,
        "CreatedOn" timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "Json" json NOT NULL,
        "ConsumedBy" varchar NOT NULL
    )

            """)
conn.commit()
cur.close()
conn.close()

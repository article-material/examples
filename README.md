## About

This repository contains code examples that I created for my articles. On the actual article you will find links to the code, here. The latest version of the code will always be under the main branch.

If you have read the article and you are interested in the code, you can simply clone this repository and check out the code.

If you have any sugestion for impovement, do not hesitate to open an issue or pull request here.

* Fork the repository.
* Create a new branch for your feature.
* Add your feature or improvement.
* Send a pull request.
* We appreciate your input!

## Links
Website [epilis.gr](https://www.epilis.gr/)
